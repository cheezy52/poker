require "rspec"
require "card"

describe "Card" do
  subject(:card) { Card.new("QH") }
  it "knows its value" do
    expect(card.value).to eq(12)
  end
  it "knows its suit" do
    expect(card.suit).to eq(:h)
  end
  it "can parse '10' cards" do
    ten_card = Card.new("10H")
    expect(ten_card.value).to eq(10)
  end
end
