require "rspec"
require "deck"
require "card"

describe "Deck" do
  subject(:deck) { Deck.new }
  it "starts with 52 unique cards" do
    expect(deck.cards.uniq.length).to eq(52)
  end
  it "can deal cards" do
    expect(deck.deal).to be_a(Card)
  end
  it "removes dealt cards from deck" do
    dealt_card = deck.deal
    expect(deck.cards.length).to eq(51)
    expect(deck.cards).to_not include(dealt_card)
  end
end