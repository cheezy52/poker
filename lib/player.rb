class Player
  attr_reader :name
  attr_accessor :hand, :is_playing, :money
  def initialize(starting_money = 500, name = "John Doe")
    @name = name
    @money = starting_money
    @hand = Hand.new
    @my_bet = 0
    @is_playing = true
  end

  def take_action(current_bet)
    begin
      puts "#{@name}, the current bet is #{current_bet}!  You are currently betting #{@my_bet} and have #{@money} left."
      puts "Your hand:  #{@hand}"
      puts "What would you like to do?  (r = raise, c = call, f = fold)"
      input = gets.chomp
      case input
      when "r"
        puts "How much would you like to raise by?"
        begin
          raise_amount = gets.chomp.to_i
          raise ArgumentError("That is not a valid raise.") if raise_amount == 0
          return raise_bet(current_bet, raise_amount)
        rescue ArgumentError => e
          puts e
          retry
        end
      when "f"
        return fold
      when "c"
        return call(current_bet)
      else
        raise ArgumentError("That is not a valid action.")
      end
    rescue ArgumentError => e
      puts e
      retry
    end
    puts "how in the world did you get here, you miscreant"
  end

  def check_discards
    begin
      puts "#{@name}, would you like to discard?"
      puts "TOO BAD"
    end
  end

  def discard(*cards)
    @hand.discard(*cards)
  end
  def receive_card(*cards)
    hand.add_cards(*cards)
  end

  def place_bet(bet)
    raise ArgumentError.new("You do not have enough money.") unless @money >= bet
    @money -= bet
    @my_bet = bet
  end

  def call(current_bet)
    diff = current_bet - @my_bet
    raise ArgumentError.new("You do not have enough money.") unless @money >= diff
    @money -= diff
    @my_bet = current_bet
    diff
  end

  def raise_bet(current_bet, raise_amount)
    diff = current_bet - @my_bet
    raise ArgumentError.new("You do not have enough money.") unless @money >= diff + raise_amount
    @money -= diff + raise_amount
    @my_bet = current_bet + raise_amount
    diff + raise_amount
  end

  def fold
    @is_playing = false
    bet = 0
  end

  def reveal_hand
    @hand
  end

  def to_s
    @name
  end
end