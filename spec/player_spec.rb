require 'rspec'
require 'player'

describe "player class" do
  subject(:player){ Player.new}
  it "is intialized without a hand" do
    expect(player.hand.cards).to be_empty
  end

  it "able to receive a hand" do
    card = Card.new('KH')
    player.receive_card(card)
    expect(player.hand.cards).to include(card)
  end

  it "can select cards to discard" do
    player.hand = Hand.new([Card.new("KH"), Card.new("JH")])
    player.discard("JH")
    expect(player.hand.cards[0].value).to eq(Card.new("KH").value)
  end

  it "has money" do
    expect(player.money).to_not be(nil)
  end

  it "can place a bet" do
    expect(player.place_bet(10)).to be(10)
    expect(player.money).to eq(Player.new.money - 10)
  end

  it "can fold" do
    player.fold
    expect(player.is_playing).to be(false)
  end

  it "can call" do
    other_player = Player.new
    current_bet = other_player.place_bet(10)
    expect(player.call(current_bet)).to be(10)
    expect(player.money).to eq(Player.new.money - 10)
  end

  it "can raise" do
    other_player = Player.new
    player.place_bet(5)
    current_bet = other_player.place_bet(10)
    expect(player.raise_bet(current_bet, 10)).to be(15)
    expect(player.money).to eq(Player.new.money - 20)
  end

  it "can reveal hand" do
    player.hand = Hand.new
    expect(player.reveal_hand).to be_a(Hand)
  end

  it "raises error on invalid input"
end