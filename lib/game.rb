require_relative 'player'
require_relative 'hand'
require_relative 'card'
require_relative 'deck'

class Game
  attr_accessor :pot, :current_bet, :player_bets, :players
  attr_reader :deck, :starting_player, :betting_player
  def initialize(players = [Player.new(500, "John Doe"), Player.new(500, "Jane of the Jungle")])
    @players = players
    @pot = 0
    @current_bet = 0
    @deck = Deck.new
    @player_bets = Hash.new(0)
    @starting_player = @players[0]
    @betting_player = @players[1]
  end

  def play
    until over?
      play_round
    end
    puts "#{winner} is the winner!"
  end

  def play_round
    set_up_round
    deal_cards
    betting_phase

    allow_discards
    refill_hands

    betting_phase
    resolve_round
    next_dealer!
  end

  def set_up_round
    @deck = Deck.new
    @current_bet = 0
    @pot = 0
    @player_bets = Hash.new(0)
    @players.each { |player| player.is_playing = true }
  end

  def deal_cards
    @players.each do |player|
      5.times do
        player.receive_card(@deck.deal)
      end
    end
  end

  def betting_phase
    call_hash = {}
    @players.each { |player| call_hash[player] = false}
    until call_hash.values.all? {|called| called == true}
      bet = @betting_player.take_action(@current_bet)
      @player_bets[@betting_player] += bet
      @pot += bet
      if @player_bets[@betting_player] == @current_bet
        call_hash[@betting_player] = true
      elsif bet == 0
        call_hash.delete(@betting_player)
      end
      if @player_bets[@betting_player] > @current_bet
        @current_bet = @player_bets[@betting_player]
      end
      #until @betting_player.is_playing
        next_better!
        #end
    end
    @player_bets.each_key do |player|
      @player_bets[player] = 0
    end
  end

  def allow_discards
    @players.each { |player| player.check_discards }
  end

  def refill_hands
    @players.each do |player|
      (5 - player.hand.cards.length).times do
        player.receive_card(@deck.deal)
      end
    end
  end

  def resolve_round
    hands = {}
    max_value = 0
    winning_player = nil
    remaining_players = @players.select { |player| player.is_playing }
    remaining_players.each do |player|
      hands[player] = player.reveal_hand
      hand_value = hands[player].value
      if hand_value > max_value
        winning_player = player
        max_value = hand_value
      end
    end
    puts "#{winning_player} wins, with a hand of #{hands[winning_player]}!"
    puts "#{winning_player} collects #{@pot}."
    winning_player.money += @pot
  end

  def next_dealer!
    @players = @players.rotate
    @starting_player = @players[0]
  end

  def next_dealer
    @players[1]
  end

  def next_better!
    if @players.index(@betting_player) == @players.length - 1
      @betting_player = @players[0]
    else
      @betting_player = @players[@players.index(@betting_player) + 1]
    end
  end

  def next_better
    if @players.index(@betting_player) == @players.length - 1
      @players[0]
    else
      @players[@players.index(@betting_player) + 1]
    end
  end

  def over?
    @players.select { |player| player.money != 0 }.length == 1
  end

  def winner
    @players.select { |player| player.money != 0 }[0]
  end

  def receive_bet(player, amount_over_last_bet)
    @pot += amount_over_last_bet
    @player_bets[player] += amount_over_last_bet
    @current_bet = @player_bets.values.max
  end
end

if __FILE__ == $PROGRAM_NAME
  g = Game.new
  g.play
end