require_relative "card"
require_relative "deck"

class Hand
  attr_accessor :cards
  def initialize(cards = [])
    @cards = cards
  end

  def add_cards(*cards)
    cards.each do |card|
      @cards << card
    end
  end

  def discard(*cards)
    if cards.length > 3
      raise ArgumentError.new("Can only discard three or fewer cards at a time.")
    else
      cards.each do |input_card|
        card = Card.new(input_card)
        @cards.delete_if  do |held_card|
          held_card.value == card.value &&
          held_card.suit == card.suit
        end
      end
    end
  end

  def draw_to_full(deck)
    until @cards.length >= 5
      @cards << deck.deal
    end
  end

  def to_s
    string = ""
    @cards.each {|card| string += card.to_s}
    string
  end

  def value
    counts = self.counts
    if straight_flush?
      10000 + kicker_value
    elsif four_of_a_kind?(counts)
      9000 + counts.key(4)*15 + make_hand_without(counts.key(4)).kicker_value
    elsif full_house?(counts)
      remaining_hand = make_hand_without(counts.key(3))
      8000 + counts.key(3)*15 + remaining_hand.counts.key(2)
    elsif flush?
      7000 + kicker_value
    elsif straight?
      6000 + kicker_value
    elsif three_of_a_kind?(counts)
      5000 + counts.key(3)*15 + make_hand_without(counts.key(3)).kicker_value
    elsif two_pair?(counts)
      first_pair_val = counts.key(2)
      one_pair_hand = make_hand_without(counts.key(2))
      counts.delete(counts.key(2))
      second_pair_val = counts.key(2)
      high_card_hand = one_pair_hand.make_hand_without(counts.key(2))

      1000 + first_pair_val*225 + second_pair_val*15 + high_card_hand.kicker_value
    elsif one_pair?(counts)
      pair_value = counts.key(2)
      remaining_hand = make_hand_without(counts.key(2))

      30 + (pair_value * 15) + remaining_hand.kicker_value
    else
      kicker_value
    end
  end

  def dup
    new_hand = Hand.new
    @cards.each do |card|
      new_hand.add_cards(card.dup)
    end
    new_hand
  end

  protected

  def make_hand_without(card_val)
    new_hand = self.dup
    new_hand.cards.delete_if {|card| card.value == card_val}
    new_hand
  end

  def kicker_value
    @cards.sort_by!{|card| card.value}.last.value
    remaining_hand = self
    kickers = []
    until remaining_hand.cards.empty?
      kickers << remaining_hand.counts.key(1)
      remaining_hand = remaining_hand.make_hand_without(remaining_hand.counts.key(1))
    end
    kicker_value = 0.0
    kickers.each_with_index do |kicker, index|
      kicker_value += kicker/(15.0**index) unless kicker.nil?
    end
    kicker_value
  end

  def counts
    counts_hash = Hash.new(0)
    @cards.sort_by! { |card| card.value }
    @cards.reverse.each do |card|
      counts_hash[card.value] += 1
    end
    counts_hash
  end

  private



  def straight_flush?
    flush? && straight?
  end

  def four_of_a_kind?(counts)
    counts.values.include?(4)
  end

  def full_house?(counts)
    counts.values.include?(3) && counts.values.include?(2)
  end

  def flush?
    suit = @cards[0].suit
    @cards.all? {|card| card.suit == suit}
  end

  def straight?
    @cards.sort_by!{|card| card.value}
    @cards.each_with_index do |card, index|
      next if index == 0
      return false unless card.value - @cards[index - 1].value == 1
    end
    true
  end

  def three_of_a_kind?(counts)
     counts.values.include?(3)
  end

  def two_pair?(counts)
     counts.values.count(2) == 2
  end

  def one_pair?(counts)
     counts.values.include?(2)
  end
end