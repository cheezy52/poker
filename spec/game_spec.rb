require 'rspec'
require 'game'

describe "Game" do
  subject(:game){Game.new}
  it "Has a deck" do
    expect(game.deck).to be_a(Deck)
  end

  it "Has a starting player" do
    expect(game.starting_player).to_not be(nil)
  end

  it "Has a betting player" do
    expect(game.betting_player).to_not be(nil)
  end

  it "can change betting player" do
    first_better = game.betting_player
    game.next_better!
    expect(game.betting_player).to_not be(first_better)
  end

  it "can change starting player" do
    first_player = game.starting_player
    game.next_dealer!
    expect(game.starting_player).to_not be(first_player)
  end


  it "has a pot" do
    expect(game.pot).to_not be(nil)
  end

  it "adds received bets to the pot" do
    player1 = Player.new
    player2 = Player.new
    game.receive_bet(player1, 10)
    game.receive_bet(player2, 15)
    game.receive_bet(player1, 10)
    expect(game.pot).to be(35)
  end

  it "keeps track of the current bet" do
    player1 = Player.new
    player2 = Player.new
    game.receive_bet(player1, 10)
    game.receive_bet(player2, 15)
    game.receive_bet(player1, 10)
    expect(game.current_bet).to be(20)
  end

  it "declares a winner when all but one player is out of money" do
    player1 = Player.new
    player2 = Player.new
    game.players = [player1, player2]
    player1.money = 0
    expect(game.over?).to be(true)
    expect(game.winner).to be(player2)
  end

  it "does does not declare a winner until only 1 player has money" do
    player1 = Player.new
    player2 = Player.new
    player3 = Player.new
    game.players = [player1, player2, player3]
    player1.money = 0
    expect(game.over?).to be(false)
  end
end