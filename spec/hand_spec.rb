require 'rspec'
require 'hand'
require 'card'
require 'deck'

describe "Hand" do
  subject(:hand){Hand.new}

  it "Starts out empty" do
    expect(hand.cards).to eq([])
  end

  it "accepts dealt cards" do
    dealt_card = Card.new("QH")
    hand.add_cards(dealt_card)
    expect(hand.cards).to include(dealt_card)
  end

  context "discarding hands starting from full hand" do
    before(:each) do
      hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("QH"),Card.new("KH"),Card.new("AH"))
    end

    it "can discard a single card" do
      hand.discard("KH")
      expect(hand.cards.map { |card| card.value }).to_not include(13)
    end

    it "can discard multiple cards" do
      hand.discard("KH", "AH")
      expect(hand.cards.length).to eq(3)
    end

    it "cannot discard more than three cards at a time" do
      expect(hand.discard("JH, QH, KH, AH")).to raise_error
    end
  end

  it "draws up to five from empty" do
    hand.draw_to_full(Deck.new)
    expect(hand.cards.length).to eq(5)
  end

  it "draws up to five with cards already in-hand" do
    hand.add_cards(Card.new("QH"))
    hand.draw_to_full(Deck.new)
    expect(hand.cards.length).to eq(5)
  end

  context "evaluates hands:" do
    let(:other_hand) {Hand.new}
    it "correctly evaluates single high card" do
      hand.add_cards(Card.new("3H"),Card.new("6D"),
      Card.new("4H"),Card.new("KH"),Card.new("2H"))

      other_hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("QH"),Card.new("7S"),Card.new("8H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 1 pair vs high card" do
      hand.add_cards(Card.new("3H"),Card.new("6D"),
      Card.new("4H"),Card.new("2H"),Card.new("2S"))

      other_hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("QH"),Card.new("7S"),Card.new("8H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 1 pair v 1 lower pair" do
      hand.add_cards(Card.new("3H"),Card.new("6D"),
      Card.new("4H"),Card.new("10H"),Card.new("3D"))

      other_hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("QH"),Card.new("2S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 1 pair v equal pair with lower kicker" do
      hand.add_cards(Card.new("3H"),Card.new("6D"),
      Card.new("4H"),Card.new("QH"),Card.new("3D"))

      other_hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("6H"),Card.new("3S"),Card.new("3H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 1 pair v equal pair with equal kickers except last" do
      hand.add_cards(Card.new("3H"),Card.new("3D"),
      Card.new("6H"),Card.new("5H"),Card.new("7D"))

      other_hand.add_cards(Card.new("7H"),Card.new("3H"),
      Card.new("3D"),Card.new("4S"),Card.new("6H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 2 pair v 1 pair" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("3H"),Card.new("3D"),Card.new("10H"))

      other_hand.add_cards(Card.new("10H"),Card.new("JH"),
      Card.new("QH"),Card.new("2S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 2 pair v 2 pair (lower highest pair)" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("3H"),Card.new("3D"),Card.new("10H"))

      other_hand.add_cards(Card.new("3H"),Card.new("3D"),
      Card.new("QH"),Card.new("2S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 2 pair v 2 pair (tied highest, lower lowest)" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("3H"),Card.new("3D"),Card.new("10H"))

      other_hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("QH"),Card.new("2S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 2 pair v 2 pair (tied pairs, lower kicker)" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("3H"),Card.new("3D"),Card.new("KH"))

      other_hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("QH"),Card.new("3S"),Card.new("3H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 3-of-a-kind v 2 pair" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("4S"),Card.new("3D"),Card.new("10H"))

      other_hand.add_cards(Card.new("3H"),Card.new("3D"),
      Card.new("QH"),Card.new("2S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 3-of-a-kind v lower 3-of-a-kind" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("4S"),Card.new("3D"),Card.new("10H"))

      other_hand.add_cards(Card.new("3H"),Card.new("3D"),
      Card.new("QH"),Card.new("3S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 3-of-a-kind v tied 3-of-a-kind w/lower kicker" do
      hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("4S"),Card.new("3D"),Card.new("KH"))

      other_hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("QH"),Card.new("4S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates straight vs 3-of-a-kind" do
      hand.add_cards(Card.new("3H"),Card.new("4D"),
      Card.new("5S"),Card.new("6D"),Card.new("7H"))

      other_hand.add_cards(Card.new("4H"),Card.new("4D"),
      Card.new("QH"),Card.new("4S"),Card.new("2H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates straight vs lower straight" do
      hand.add_cards(Card.new("3H"),Card.new("4D"),
      Card.new("5S"),Card.new("6D"),Card.new("7H"))

      other_hand.add_cards(Card.new("2H"),Card.new("3D"),
      Card.new("4H"),Card.new("5S"),Card.new("6H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates flush vs straight" do
      hand.add_cards(Card.new("3H"),Card.new("4H"),
      Card.new("5H"),Card.new("6H"),Card.new("8H"))

      other_hand.add_cards(Card.new("2H"),Card.new("3D"),
      Card.new("4H"),Card.new("5S"),Card.new("6H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates flush vs flush w/lower kicker" do
      hand.add_cards(Card.new("3H"),Card.new("4H"),
      Card.new("5H"),Card.new("6H"),Card.new("KH"))

      other_hand.add_cards(Card.new("QH"),Card.new("JH"),
      Card.new("10H"),Card.new("9H"),Card.new("7H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates full house vs flush" do
      hand.add_cards(Card.new("4H"),Card.new("4S"),
      Card.new("4D"),Card.new("3H"),Card.new("3S"))

      other_hand.add_cards(Card.new("QH"),Card.new("JH"),
      Card.new("10H"),Card.new("9H"),Card.new("7H"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates full house vs full house w/lower 3-of-a-kind" do
      hand.add_cards(Card.new("4H"),Card.new("4S"),
      Card.new("4D"),Card.new("3H"),Card.new("3S"))

      other_hand.add_cards(Card.new("3H"),Card.new("3S"),
      Card.new("3D"),Card.new("QH"),Card.new("QD"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 4-of-a-kind vs full house" do
      hand.add_cards(Card.new("3H"),Card.new("3S"),
      Card.new("3D"),Card.new("3C"),Card.new("JC"))

      other_hand.add_cards(Card.new("JH"),Card.new("JS"),
      Card.new("JD"),Card.new("QH"),Card.new("QD"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates 4-of-a-kind vs lower 4-of-a-kind" do
      hand.add_cards(Card.new("3H"),Card.new("3S"),
      Card.new("3D"),Card.new("3C"),Card.new("JC"))

      other_hand.add_cards(Card.new("2H"),Card.new("2S"),
      Card.new("2D"),Card.new("2H"),Card.new("QD"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates straight-flush vs 4-of-a-kind" do
      hand.add_cards(Card.new("3H"),Card.new("4H"),
      Card.new("5H"),Card.new("6H"),Card.new("7H"))

      other_hand.add_cards(Card.new("KH"),Card.new("KS"),
      Card.new("KD"),Card.new("KH"),Card.new("QD"))

      expect(hand.value).to be > other_hand.value
    end
    it "correctly evaluates straight-flush vs lower straight-flush" do
      hand.add_cards(Card.new("3H"),Card.new("4H"),
      Card.new("5H"),Card.new("6H"),Card.new("7H"))

      other_hand.add_cards(Card.new("2C"),Card.new("3C"),
      Card.new("4C"),Card.new("5C"),Card.new("6C"))

      expect(hand.value).to be > other_hand.value
    end
  end
end