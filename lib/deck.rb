require_relative  'card'
class Deck
  attr_reader :cards
  def initialize
    @cards = []
    fill_deck
  end
  def deal
    @cards.pop
  end
  private
  def fill_deck
    Card::VALUES.each_key do |key|
      Card::SUITS.each_key do |suit|
        @cards << Card.new("#{key}#{suit}")
      end
    end
    @cards = @cards.shuffle
  end
end