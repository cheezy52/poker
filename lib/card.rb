class Card
  VALUES = {"2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8,
    "9" => 9, "10" => 10, "J" => 11, "Q" => 12, "K" => 13, "A" => 14}
  SUITS = {"H" => :h, "D" => :d, "C" => :c, "S" => :s}

  attr_accessor :value, :suit
  def initialize(type)
    @value = VALUES[type[0...-1].upcase]
    @suit = SUITS[type[-1].upcase]
  end

  def to_s
    "#{VALUES.key(@value)}#{SUITS.key(@suit)} "
  end
end